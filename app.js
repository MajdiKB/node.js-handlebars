require('dotenv').config();
const express = require('express');

require('./config/db');
require('./config/passport');

const path = require('path');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');

const app = express();
const PORT = process.env.PORT || 3000;

const { DB_URL } = require('./config/db');

const homeRoute = require('./routes/home.route');
const apiRoute = require('./routes/api.route');
const restaurantRoute = require('./routes/restaurant.route');
const clientRoute = require('./routes/client.route');
const reservationRoute = require('./routes/reservation.route');

const authMiddleware = require('./middlewares/auth.middleware');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
    session({
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 3600000,
      },
      store: MongoStore.create({
        mongoUrl: DB_URL,
      }),
    }),
);

app.use(passport.initialize());
app.use(passport.session());

// Routes:
app.use('/', homeRoute);
app.use('/api', apiRoute);
app.use('/restaurant', [authMiddleware.isAuthenticated], restaurantRoute);
app.use('/client', [authMiddleware.isAuthenticated], clientRoute);
app.use('/reservation', reservationRoute);

app.use(express.static(path.join(__dirname, 'public')));

app.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
});

app.use((err, req, res) => {
  return res.status(err.status || 500).render('error', {
    message: err.message || 'Unexpected error',
    status: err.status || 500,
  });
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.listen(PORT, () => {
  console.log(`Listening in http://localhost:${PORT}`);
});
