const mongoose = require('mongoose');
// const DB_URL = 'mongodb://localhost:27017/node-project';
const DB_URL = process.env.DB_URL;

const connection = mongoose
    .connect(DB_URL, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log('Connected to DB!');
    })
    .catch((err) => {
      console.log('Error connecting to DB:', err.message);
    });

module.exports = { connection, DB_URL };
