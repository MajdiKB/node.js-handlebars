const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('../models/User');

const saltRounds = 10;

passport.use(
    'register',
    new LocalStrategy(
        {
          usernameField: 'email',
          passwordField: 'password',
          passReqToCallback: true,
        },

        async (req, email, password, done) => {
          try {
            const previousUser = await User.findOne({ email: email });
            if (previousUser) {
              const error = new Error('The user is already registered');
              return done(error);
            }
            // const photo = req.file ? req.file.filename : null;
            const photo = req.file_url || null;

            const hash = await bcrypt.hash(password, saltRounds);
            const newUser = new User({
              name: req.body.name,
              email: email,
              password: hash,
              role: req.body.role,
              photo: photo,
            });
            const savedUser = await newUser.save();
            console.log('user registrado');
            done(null, savedUser);
          } catch (err) {
            return done(err);
          }
        },
    ),
);

passport.use(
    'login',
    new LocalStrategy(
        {
          usernameField: 'email',
          passwordField: 'password',
          passReqToCallback: true,
        },
        async (req, email, password, done) => {
          try {
            const currentUser = await User.findOne({ email: email });

            if (!currentUser) {
              const error = new Error('The user does not exist!');
              return done(error);
            }

            const isValidPassword = await bcrypt.compare(password, currentUser.password);

            if (!isValidPassword) {
              const error = new Error('The username & password combination is incorrect!');
              return done(error);
            }

            done(null, currentUser);
          } catch (err) {
            return done(err);
          }
        },
    ),
);

passport.serializeUser(async (user, done) => {
  const userId = await user._id;
  return done(null, userId);
});

passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});
