// eslint-disable-next-line require-jsdoc
function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    return res.redirect('/api/login');
  }
}

module.exports = { isAuthenticated };
