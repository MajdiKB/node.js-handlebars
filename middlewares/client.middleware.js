// eslint-disable-next-line require-jsdoc
function isClient(req, res, next) {
  console.log(`El usuario es un: ${req.user.role}`);

  if (req.user.role != 'client') {
    return res.redirect('/auth/login');
  } else {
    return next();
  }
}
module.exports = { isClient };
