// eslint-disable-next-line require-jsdoc
function isUser(req, res, next) {
  if (req.user) {
    console.log(`ID Usuario : ${req.user.id}`);
    console.log(`ID parametro : ${req.params.id}`);
    if (req.user.id != req.params.id) {
      console.log('No puede ver el perfil, los ids no coinciden');
      return res.redirect(`/api/login`);
    } else {
      return next();
    }
  }
}
module.exports = { isUser };
