const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
    {
      clientId: { type: mongoose.Types.ObjectId, ref: 'User', required: true },
      clientName: { type: String, required: true },
      restaurantId: { type: mongoose.Types.ObjectId, ref: 'User', required: true },
      restaurantName: { type: String, required: true },
      reservationDate: { type: String },
      reservationTime: { type: String },
      persons: { type: Number },
      photo: { type: String },
    },
    {
      timestamps: true,
    },
);

const Reservation = mongoose.model('Reservation', reservationSchema);
module.exports = Reservation;
