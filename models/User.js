const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
      name: { type: String, required: true },
      email: { type: String, required: true },
      password: { type: String, required: true },
      role: { type: String, required: true },
      location: { type: String },
      phone: { type: Number },
      tables: { type: Number },
      persons: { type: Number },
      photo: { type: String },
    // reservations: [{ type: mongoose.Types.ObjectId, ref: 'Reservation' }],
    },
    {
      timestamps: true,
    },
);

const User = mongoose.model('User', userSchema);
module.exports = User;
