const showRestaurant = async () => {
  const restaurantList = document.getElementById('restaurant-list');
  const restaurantListItem = document.getElementById('restaurant-list');
  // intentar q coja los valores del select y no del options a ver si funciona
  const restauranteId = restaurantList.value;
  const restaurantName = restaurantListItem.textContent;
  const restaurantEmail = restaurantListItem.className;
  const restaurantPhone = restaurantListItem.title;

  document.getElementById('restaurantdetails').classList.remove('hidden');

  const id = document.getElementById('id-restaurant');
  const name = document.getElementById('name-restaurant');
  const email = document.getElementById('email-restaurant');
  const phone = document.getElementById('phone-restaurant');

  id.innerText = `Id: ${restauranteId}`;
  name.innerText = `Name: ${restaurantName}`;
  email.innerText = `Email: ${restaurantEmail}`;
  phone.innerText = `Phone: ${restaurantPhone}`;
};

window.onload = () => {
  const restaurantList = document.getElementById('restaurant-list');
  document.getElementById('restaurantdetails').classList.add('hidden');
  restaurantList.addEventListener('change', showRestaurant);
};
