const express = require('express');
const passport = require('passport');

const User = require('../models/User');
const Reservation = require('../models/Reservation');

// eslint-disable-next-line new-cap
const router = express.Router();

const fileMiddlewares = require('../middlewares/file.middleware');

router.get('/register', (req, res, next) => {
  try {
    if (req.user) {
      console.log('ya hay usuario logeado');
      if (req.user.role === 'client') {
        res.status(200).redirect(`/client/details/${req.user.id}`);
      }
      if (req.user.role === 'restaurant') {
        res.status(200).redirect(`/restaurant/details/${req.user.id}`);
      }
    } else {
      res.status(200).render('register', { tittle: 'Register' });
    }
  } catch (err) {
    next(err);
  }
});

router.post(
    '/register',
    [fileMiddlewares.upload.single('photo')],
    [fileMiddlewares.uploadToCloudinary],
    (req, res, next) => {
      if (!req.user) {
        passport.authenticate('register', (error, user) => {
          if (error) {
            return res.render('error', { error: error });
          }
          req.logIn(user, async (err) => {
            if (err) {
              return res.render('register', { error: error });
            }
            if (req.body.role === 'client') {
              const clientId = req.user.id;
              const reservations = await Reservation.find({ clientId: clientId });
              const client = await User.findById(clientId);
              res.status(200).render('client', {
                title: client.name,
                reservations,
                client,
                clientId,
              });
            }
            if (req.body.role === 'restaurant') {
              const restaurantId = req.user.id;
              const restaurant = await User.findById(restaurantId);
              return res
                  .status(200)
                  .render(`editrestaurant`, { title: restaurant, restaurant, restaurantId });
            }
          });
        })(req, res, next);
      } else {
        console.log('ya hay usuario logeado');
        if (req.user.role === 'client') {
          res.status(200).redirect(`/client/details/${req.user.id}`);
        }
        if (req.user.role === 'restaurant') {
          res.status(200).redirect(`/restaurant/details/${req.user.id}`);
        }
      }
    },
);

router.get('/login', async (req, res, next) => {
  try {
    if (req.user) {
      console.log('ya hay usuario logeado');
      if (req.user.role === 'client') {
        res.status(200).redirect(`/client/details/${req.user.id}`);
      }
      if (req.user.role === 'restaurant') {
        res.status(200).redirect(`/restaurant/details/${req.user.id}`);
      }
    } else {
      res.render('login');
    }
  } catch (err) {
    next(err);
  }
});

router.post('/login', (req, res, next) => {
  passport.authenticate('login', (error, user) => {
    if (error) {
      return res.render('error', { error: error });
    }

    req.logIn(user, async (err) => {
      if (err) {
        return res.render('error', { error: error });
      }
      const currentUserId = req.user.id;
      const currentUserRole = req.user.role;
      if (currentUserRole === 'client') {
        const reservations = await Reservation.find({ clientId: currentUserId });
        const client = await User.findById(currentUserId);
        const clientId = req.user.id;
        res.status(200).render('client', {
          title: 'Reservation',
          reservations,
          client,
          clientId,
        });
      }
      if (currentUserRole === 'restaurant') {
        const restaurantId = currentUserId;
        const restaurant = await User.findById(restaurantId);
        const reservations = await Reservation.find({ restaurantId: restaurantId });
        return res.status(200).render(`restaurant`, { restaurant, reservations });
      }
    });
  })(req, res, next);
});

router.get('/logout', (req, res, next) => {
  if (req.user) {
    req.logout();

    req.session.destroy(() => {
      res.clearCookie('connect.sid');
      console.log('Correct User logout');
      return res.status(200).redirect('/');
    });
  } else {
    console.log('No user logged');
    return res.status(200).redirect('/');
  }
});

// Aquí funciona el post sin passport

// router.post('/register', async (req, res, next) => {
//   try {
//     const newUser = new User({
//       name: req.body.name,
//       location: req.body.location,
//       phone: req.body.phone,
//       email: req.body.email,
//       password: req.body.password,
//       role: req.body.role,
//     });
//     await newUser.save();
//     if (req.body.role==='client') {
//       const users = await User.find({'role': 'client'});
//       return res.status(200).json(users);
//     }
//     if (req.body.role==='restaurant') {
//       const users = await User.find({'role': 'restaurant'});
//       return res.status(200).json(users);
//     }
//   } catch (err) {
//     next(err);
//   }
// });
module.exports = router;
