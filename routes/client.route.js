const express = require('express');
const User = require('../models/User');
const Reservation = require('../models/Reservation');

// eslint-disable-next-line new-cap
const router = express.Router();

const idMiddleware = require('../middlewares/id.middleware');
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddlewares = require('../middlewares/file.middleware');

router.get('/', async (req, res, next) => {
  try {
    const clients = await User.find({ role: 'client' }, { password: 0 });
    return res.status(200).json(clients);
  } catch (err) {
    next(err);
  }
});

router.get('/details/:id', [idMiddleware.isUser], async (req, res, next) => {
  try {
    const clientId = req.user.id;
    const client = await User.findById(clientId);
    const reservations = await Reservation.find({ clientId: clientId });
    return res.status(200).render('client', { client: client, reservations, clientId });
  } catch (err) {
    next(err);
  }
});

router.get('/reservation', [authMiddleware.isAuthenticated], async (req, res, next) => {
  if (req.user.role === 'client') {
    const restaurants = await User.find({ role: 'restaurant' });
    const currentUser = req.user.name;
    const currentUserId = req.user.id;
    console.log(req.user.id);

    res
        .status(200)
        .render('gotoreservation', {
          title: 'Reservation',
          restaurants,
          currentUser,
          currentUserId,
          clientId: currentUserId,
        });
  }
  if (req.user.role === 'restaurant') {
    console.log('Only for clients. No restaurants');
    res.status(200).render('login');
  }
});

// para renderizar el editclient
router.get('/update', async (req, res, next) => {
  try {
    const clientId = req.user.id;
    const client = await User.findById(clientId);
    return res.status(200).render(`editclient`, { title: 'Edit Client', client, clientId });
  } catch (err) {
    next(err);
  }
});

router.post(
    '/update',
    [fileMiddlewares.upload.single('photo')],
    [fileMiddlewares.uploadToCloudinary],
    async (req, res, next) => {
      try {
        console.log(req.body.name);
        const clientId = req.body.clientId;
        if (req.file) {
        // const photo = req.file ? req.file.filename : null;
          const photo = req.file_url || null;
          await User.findByIdAndUpdate(
              clientId,
              {
                name: req.body.name,
                photo: photo,
              },
              { new: true },
          );
        // si sólo cambia el nombre pero misma foto
        } else {
          await User.findByIdAndUpdate(
              clientId,
              {
                name: req.body.name,
              },
              { new: true },
          );
        }
        const client = await User.findById(clientId);
        const reservations = await Reservation.find({ clientId: clientId });
        return res.status(200).render('client', { client, reservations, clientId });
      } catch (err) {
        next(err);
      }
    },
);

// funciona como un delete
router.get('/delete', async (req, res, next) => {
  try {
    const clientId = req.user.id;
    await Reservation.deleteMany({ clientId: clientId });
    await User.findByIdAndDelete(clientId);
    return res.status(200).render('deleteclient');
  } catch (err) {
    next(err);
  }
});

module.exports = router;
