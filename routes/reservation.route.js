const express = require('express');
const Reservation = require('../models/Reservation');
const User = require('../models/User');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const reservation = await Reservation.find({});
    return res.status(200).json(reservation);
  } catch (err) {
    next(err);
  }
});

router.get('/details/:id', async (req, res, next) => {
  const restaurantName = req.params.id;
  try {
    const reservation = await Reservation.find({ _id: restaurantName });
    return res.status(200).json(reservation);
  } catch (err) {
    next(err);
  }
});

router.get('/client/', async (req, res, next) => {
  try {
    const userId = req.user.id;
    const reservations = await Reservation.find({ clientId: userId });
    return res.status(200).render('clientreservations', { reservations });
  } catch (err) {
    next(err);
  }
});

router.post('/add', async (req, res, next) => {
  try {
    const clientId = req.user.id;
    const clientName = req.user.name;

    const restaurantId = req.body.restaurant;
    const restaurantName = await User.findById(restaurantId);

    const reservationDate = req.body.date;
    const reservationTime = req.body.time;

    const persons = req.body.persons;

    const newReservation = new Reservation({
      clientId: clientId,
      clientName: clientName,
      restaurantId: restaurantId,
      restaurantName: restaurantName.name,
      reservationDate: reservationDate,
      reservationTime: reservationTime,
      persons: persons,
      photo: restaurantName.photo,
    });

    await newReservation.save();
    // const newReservationId = await Reservation.findById(newReservation.id);
    //  aquí metiamos las reservas en un array en la colección usuarios
    // se eliminó ese array del modelo usuario porq no estaba bien estructurado
    // await User.findByIdAndUpdate(
    //     clientId,
    //     { $push: { reservations: newReservationId } },
    //     { new: true },
    // );
    return res.status(200).render('newreservation', { reservation: newReservation });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
