const express = require('express');

const User = require('../models/User');

// eslint-disable-next-line new-cap
const router = express.Router();

const idMiddleware = require('../middlewares/id.middleware');
const fileMiddlewares = require('../middlewares/file.middleware');
const clientMiddleware = require('../middlewares/client.middleware');

const Reservation = require('../models/Reservation');

router.get('/', [clientMiddleware.isClient], async (req, res, next) => {
  try {
    const clientId = req.user.id;
    console.log(clientId);
    const restaurants = await User.find({ role: 'restaurant' });
    return res.status(200).render('allrestaurants', { restaurants, clientId });
  } catch (err) {
    next(err);
  }
});

router.get('/details/:id', [idMiddleware.isUser], async (req, res, next) => {
  const restaurantId = req.user.id;
  try {
    const restaurant = await User.findById({ _id: restaurantId });
    const reservations = await Reservation.find({ restaurantId: restaurantId });
    return res.status(200).render('restaurant', { restaurant, reservations });
  } catch (err) {
    next(err);
  }
});

// para renderizar el editrestaurant
router.get('/update', async (req, res, next) => {
  try {
    const restaurantId = req.user.id;
    const restaurant = await User.findById(restaurantId);
    return res
        .status(200)
        .render(`editrestaurant`, { title: 'restaurant', restaurant, restaurantId });
  } catch (err) {
    next(err);
  }
});

router.post(
    '/update',
    [fileMiddlewares.upload.single('photo')],
    [fileMiddlewares.uploadToCloudinary],
    async (req, res, next) => {
      try {
        const restaurantId = req.body.restaurantId;
        if (req.file) {
        // const photo = req.file ? req.file.filename : null;
          const photo = req.file_url || null;
          await User.findByIdAndUpdate(
              restaurantId,
              {
                phone: req.body.phone,
                location: req.body.location,
                tables: req.body.tables,
                persons: req.body.persons,
                photo: photo,
              },
              { new: true },
          );
          await Reservation.updateMany(
              { restaurantId: restaurantId },
              {
                photo: photo,
              },
          );
        } else {
          await User.findByIdAndUpdate(
              restaurantId,
              {
                phone: req.body.phone,
                location: req.body.location,
                tables: req.body.tables,
                persons: req.body.persons,
              },
              { new: true },
          );
        }
        const restaurant = await User.findById(restaurantId);
        const reservations = await Reservation.find({ restaurantId: restaurantId });
        console.log('restaurante actualizado');
        return res.status(200).render('restaurant', { restaurant, reservations });
      } catch (err) {
        next(err);
      }
    },
);

// funciona como un delete
router.get('/delete', async (req, res, next) => {
  try {
    const restaurantId = req.user.id;
    await Reservation.deleteMany({ restaurantId: restaurantId });
    await User.findByIdAndDelete(restaurantId);
    return res.status(200).render('deleterestaurant');
  } catch (err) {
    next(err);
  }
});

module.exports = router;
